
public class Team 
{
	public enum PlayerType 
	{
	 PLAYER_ONE(0), PLAYER_TWO(1);
		 
	 private final int _id;
	 PlayerType (int id) { _id = id;}
	 public int GetValue() { return _id; }
	}

	Player _players[] = new Player[2];
	int _score;
	int _nbPlayersInTeam;
	
	public Team()
	{ 
		_players[0] = new Player();
		_players[1] = new Player();
		_score = 0; 
		_nbPlayersInTeam = 0;	
	}
	
	public void AddScore(int score, PlayerType type) { _players[type.GetValue()].SetScore(score); }
	public int GetScore(){ return _score; }
	
	public int GetPlayerScore(PlayerType type) { return _players[type.GetValue()].GetScore();}
	public Player GetPlayer(PlayerType type) { return _players[type.GetValue()]; }				
	public Player[] GetPlayerList() { return _players; }
	
	public boolean AddPlayer(String name) 
	{ 
		if (_nbPlayersInTeam == 2)
			return false;
		if (_players[0].GetName() == "")
			_players[0].SetName(name);
		else
			_players[_nbPlayersInTeam].SetName(name);
		_nbPlayersInTeam++;
		return true;
	}
	
	public boolean RemovePlayer(String name) 
	{ 
		if (_nbPlayersInTeam == 0)
			return false;
		int cnt = _nbPlayersInTeam;
		while (cnt > 0)
		{
			if (_players[cnt - 1].GetName() == name)
			{
				_players[cnt - 1].SetName("");
				_players[cnt - 1].SetScore(0);
				_nbPlayersInTeam--;
				return true;
			}
			cnt--;
		}
		return false;
	}
}
