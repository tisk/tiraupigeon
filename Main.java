
public class Main {

	public static void unitTestClassRoomTeamPlayer()
	{
		Room test = new Room();
		
		test.AddPlayer("billi", Room.TeamNumber.TEAM_ONE);
		test.AddPlayer("bob", Room.TeamNumber.TEAM_TWO);
		test.AddPlayer("billi2", Room.TeamNumber.TEAM_ONE);
		test.AddPlayer("bob2", Room.TeamNumber.TEAM_TWO);
				
		Team team1 = test.GetTeam(Room.TeamNumber.TEAM_ONE);
		Team team2 = test.GetTeam(Room.TeamNumber.TEAM_TWO);
		
		System.out.println("_________TEAM ONE__________");
		System.out.println("____PLAYER ONE NAME : _____");
		System.out.println(team1.GetPlayer(Team.PlayerType.PLAYER_ONE).GetName());
		System.out.println("____PLAYER TWO NAME : _____");
		System.out.println(team1.GetPlayer(Team.PlayerType.PLAYER_TWO).GetName());
		
		System.out.println("_________TEAM TWO__________");
		System.out.println("____PLAYER ONE NAME : _____");
		System.out.println(team2.GetPlayer(Team.PlayerType.PLAYER_ONE).GetName());
		System.out.println("____PLAYER TWO NAME : _____");
		System.out.println(team2.GetPlayer(Team.PlayerType.PLAYER_TWO).GetName());
		
		if (!test.AddPlayer("billi3", Room.TeamNumber.TEAM_ONE))
			System.out.println("can't add player TEAM_ONE is full");
		if (!test.AddPlayer("bob3", Room.TeamNumber.TEAM_TWO))
			System.out.println("can't add player TEAM_TWO is full");
		
		
		
		test.RemovePlayer("billi", Room.TeamNumber.TEAM_ONE);
		System.out.println("\n\nRemove billi from TEAM ONE :");
		System.out.println("PLAYER ONE NAME :");
		System.out.println(team1.GetPlayer(Team.PlayerType.PLAYER_ONE).GetName());
		System.out.println("PLAYER TWO NAME :");
		System.out.println(team1.GetPlayer(Team.PlayerType.PLAYER_TWO).GetName());
		if (!test.AddPlayer("Billi Bob", Room.TeamNumber.TEAM_ONE))
			System.out.println("can't add player in TEAM_ONE. this is not normal....");
		
		System.out.println("\n\n_________ADD billi bob in TEAM ONE__________");
		System.out.println("____PLAYER ONE NAME : _____");
		System.out.println(team1.GetPlayer(Team.PlayerType.PLAYER_ONE).GetName());
		System.out.println("____PLAYER TWO NAME : _____");
		System.out.println(team1.GetPlayer(Team.PlayerType.PLAYER_TWO).GetName());
	}
	
	
	
	public static void main(String[] args) {
		unitTestClassRoomTeamPlayer();
	}

}
