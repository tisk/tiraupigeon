
public class Room {
	public enum TeamNumber 
	{
	 TEAM_ONE(0), TEAM_TWO(1);
		 
	 private final int _id;
	 TeamNumber(int id) { _id = id;}
	 public int GetValue() { return _id; }
	}
	
	
	private Team _teams[] = new Team[2];
	public boolean _isReadyToRun = false;
	
	public Room() { _teams[0] = new Team(); _teams[1] = new Team(); }
	
	public boolean AddPlayer(String name, TeamNumber teamNumber) { return _teams[teamNumber.GetValue()].AddPlayer(name); }	
	public boolean RemovePlayer(String name, TeamNumber teamNumber) { return _teams[teamNumber.GetValue()].RemovePlayer(name); }
	public boolean ReadyToRun(){ return _isReadyToRun; }
	
	public Team GetTeam(TeamNumber teamNumber) {return _teams[teamNumber.GetValue()];}
	public Team[] GetTeamList() { return _teams; }
	
}
