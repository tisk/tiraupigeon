import java.io.IOException;

public class Main_Test
{
	static boolean flagEnd;
	static Client _client;
	
	private static void act(String cmd)
	{
	    System.out.printf("cmd = %s$\n", cmd);
		if (cmd.length() == 4 && cmd.compareTo("quit") == 0)
			flagEnd = true;
		if (cmd.length() >= 16 && cmd.substring(0, 6).compareTo("client") == 0)
		{
			String s = new String(""), s2 = new String("");
			
			int pos = 6;
			while (cmd.charAt(pos) == ' ')
				pos++;
			while (pos < cmd.length() && cmd.charAt(pos) != ' ')
				s += cmd.charAt(pos++);
			while (cmd.charAt(pos) == ' ')
				pos++;
			while (pos < cmd.length() && cmd.charAt(pos) != ' ')
				s2 += cmd.charAt(pos++);
			if (_client.joinRoom(s, Integer.decode(s2)) == true)
			    System.out.printf("Connect to server\n");
			else
				System.out.printf("Connect to server Failed\n");
				
		}
		if (cmd.length() >= 8 && cmd.substring(0, 6).compareTo("server") == 0)
		{
			String s = new String("");
			
			int pos = 6;
			while (cmd.charAt(pos) == ' ')
				pos++;
			while (pos < cmd.length() && cmd.charAt(pos) != ' ')
				s += cmd.charAt(pos++);
			if (_client.createRoom(Integer.decode(s)) == true)
			    System.out.printf("Create server\n");
			else
			    System.out.printf("Create server Failed\n");
		}
		if (cmd.length() >= 12 && cmd.substring(0, 11).compareTo("changeTeam ") == 0)
		{
			String s = new String("");

			int pos = 10;
			while (pos < cmd.length() && cmd.charAt(pos) == ' ')
				pos++;
			while (pos < cmd.length() && cmd.charAt(pos) != ' ')
				s += cmd.charAt(pos++);
			_client.changeTeam(Integer.decode(s));
		}
		if (cmd.length() >= 12 && cmd.substring(0, 11).compareTo("changeName ") == 0)
		{
			String s = new String("");

			int pos = 10;
			while (pos < cmd.length() && cmd.charAt(pos) == ' ')
				pos++;
			while (pos < cmd.length())
				s += cmd.charAt(pos++);
			_client.changeName(s);
		}
		if (cmd.length() >= 7 && cmd.substring(0, 7).compareTo("runGame") == 0)
		{
			_client.runGame();
		}
		if (cmd.length() >= 4 && cmd.substring(0, 4).compareTo("Fire") == 0)
		{
			_client.clickFire();
		}
		if (cmd.length() >= 6 && cmd.substring(0, 6).compareTo("Shoot ") == 0)
		{
			String s = new String("");
			String s2 = new String("");

			int pos = 5;
			while (pos < cmd.length() && cmd.charAt(pos) == ' ')
				pos++;
			while (pos < cmd.length())
				s += cmd.charAt(pos++);
			while (pos < cmd.length() && cmd.charAt(pos) == ' ')
				pos++;
			while (pos < cmd.length())
				s2 += cmd.charAt(pos++);
			_client.ClickShoot(Integer.decode(s), Integer.decode(s2));
		}
	}
	
	  public static void main(String[] args)
	  {
		  String cmd = new String("");
		  _client = new Client();
		  
		  flagEnd = false;
		  while (flagEnd == false)
		  {
			  try
			  {
				  byte[] array = new byte[200];
				  int i = System.in.read(array);
				  cmd = "";
				  for (int j = 0; j < (i - 2); j++)
				  {
				   	cmd += "" +(char)array[j];
				  }
				  act(cmd);
			  }
	          catch(IOException e)
			  {
	      	    System.out.println("Error");
			  }
		  }
	  }
}
