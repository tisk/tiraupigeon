
public class Client
{
	public enum gameState
	{
		Menu,
		Room,
		Game,
	}
	
	Network _network;
	Room _room;
	gameState _gameState;
	Player _yourInfo;
	int _yourTeam;
	Game _game;
	
	Client()
	{
		_network = new Network(this);
		_room = new Room();
		_gameState = gameState.Room;
		_yourInfo = new Player();
		_yourTeam = 0;
	}
	
	public boolean createRoom(int port)
	{
		return _network.createServer(port);
	}

	public boolean joinRoom(String addr, int port)
	{
		return _network.connectToServer(addr, port);
	}
	
	public void quitRoom(String reason)
	{
	}
	
	public void runGame() // Utilisateur : lancer la partie
	{
		if (_network != null)
			_network.runGame();
	}

	public void quitRoom() // Utilisateur : quitter le jeu
	{
		if (_network != null)
			_network.quitRoom();
	}

	public void changeName(String name) // Utilisateur : quitter le jeu
	{
		if (_network != null)
			_network.changeName(name);
	}

	public void changeTeam() // Utilisateur : Changer l'�quipe
	{
		if (_network != null)
			_network.changeTeam((_yourTeam == 1 ? 2 : 1));
	}
	
	public void changeTeam(int team) // Utilisateur : changer le nom
	{
		if (_network != null)
			_network.changeTeam(team);
	}

	public void clickFire() // Utilisateur : Bouton Fire
	{
		_network.sendFire();
	}

	public void ClickShoot(int x, int y) // Utilisateur : Tire du joueur
	{
		_network.sendShoot(x, y);
	}
	
	public void launchGame() // lancement de la partie sans la boucle de jeu (sert uniquement de conteneur pour les donn�es
	{
		_gameState = Client.gameState.Game;
		_game = new Game(_room, this);
	}

	public void launchGameServer() // lancement de la partie avec la boucle de jeu
	{
		_gameState = Client.gameState.Game;
		_game = new Game(_room, this);
		_game.start();
	}
	
	public void endGame(int scoreTeam1, int scoreTeam2, int idTeamWin)
	{
		_gameState = Client.gameState.Menu;
		if (_game.isAlive() == true)
			_game.endGame();
		_game = null;
	}

	public void Fire()
	{
		_game.Fire();
	}

	public void Fire(boolean isDouble, boolean isPull)
	{
		_network.Fire(isDouble, isPull);
	}
	
	public void sendEndGame(int score1, int score2, int teamWin)
	{
		_network.sendEndGame(score1, score2, teamWin);
	}

	public void changeScoreTeam(int score, String player)
	{
		_network.changeScoreTeam(score, player);
	}

	public void killTarget(int id)
	{
		_network.killTarget(id);
	}

	public void outTarget(int id)
	{
		_network.outTarget(id);
	}
	
	public void mvtTarget(int id, int x, int y)
	{
		_network.mvtTarget(id, x, y);

	    System.out.printf(_game.toString());
	}
	
	public void updateRoom(String team1Player1, String team1Player2, String team2Player1, String team2Player2)
	{
		_room.GetTeam(Room.TeamNumber.TEAM_ONE).GetPlayer(Team.PlayerType.PLAYER_ONE).SetName(team1Player1);
		_room.GetTeam(Room.TeamNumber.TEAM_ONE).GetPlayer(Team.PlayerType.PLAYER_TWO).SetName(team1Player2);
		_room.GetTeam(Room.TeamNumber.TEAM_TWO).GetPlayer(Team.PlayerType.PLAYER_ONE).SetName(team2Player1);
		_room.GetTeam(Room.TeamNumber.TEAM_TWO).GetPlayer(Team.PlayerType.PLAYER_TWO).SetName(team2Player2);
	}
	
	public void changeStation(int station)
	{
		_network.changeStation(station);
	}
	
	public void changePlayerTurn(String name)
	{
		_network.changePlayerTurn(name);
	}
	
	public Network getNetwork()
	{
		return _network;
	}

	public Room getRoom()
	{
		return _room;
	}
	
	public Game getGame()
	{
		return _game;
	}

	public Player getYourInfo()
	{
		return _yourInfo;
	}

	public Client.gameState getGameState()
	{
		return _gameState;
	}
	
	public void setTeam(int team)
	{
		_yourTeam = team;
	}
	
	/* R�ception des de la partie donn�es du serveur d�di� uniquement a l'utilisation dans l'affichage et/ou effet sonores*/
	
	public void playerShootReceive(int x, int y)
	{
		_game.playerShootReceive(x, y);
	}
	
	public void MvtTargetReceive(int id, int x, int y)
	{
		_game.MvtTargetReceive(id, x, y);
	}
	
	public void KillTargetReceive(int id)
	{
		_game.KillTargetReceive(id);
	}
	
	public void OutTargetReceive(int id)
	{
		_game.OutTargetReceive(id);
	}
	
	public void ChangeScoreReceive(String player, int score)
	{
		_game.ChangeScoreReceive(player, score);
	}
	
	public void PlayerTurnReceive(String player)
	{
		_game.PlayerTurnReceive(player);
	}
	
	public void EndGameReceive(int scoreTeam1, int scoreTeam2, int teamWin)
	{
		_game.EndGameReceive(scoreTeam1, scoreTeam2, teamWin);
	}
	
	public void PosForShootReceive(int pos)
	{
		_game.PosForShootReceive(pos);
	}
	
	public void PullReceive()
	{
		_game.PullReceive();
	}
	
	public void MarkReceive()
	{
		_game.MarkReceive();
	}
	
	public void DoubleReceive()
	{
		_game.DoubleReceive();
	}
}
