
public class Target
{
	private int _id;
	private double _x;
	private double _y;
	private boolean _fromPull;
	private double _vx;
	private double _vy;
	private static final double gravity = 0.2;
	private static final int speed = 10;
	public static final int radius = 10;
	
	public Target(int id, double dir, boolean fromPull)
	{
		_id = id;
		_fromPull = fromPull;
		if (fromPull)
		{
			_x = 0;
			_y = Game.screenHeight / 2;
		}
		else
		{
			_x = Game.screenWidth;
			_y = Game.screenHeight - 100;
		}
		_vx = Math.cos(dir) * speed;
		_vx = -Math.sin(dir) * speed;
	}
	
	public int get_id() {
		return _id;
	}

	public boolean is_fromPull() {
		return _fromPull;
	}

	public void updatePosition()
	{
		_x += _vx;
		_y += _vy;
		_vy += gravity;
	}

	public double get_x() {
		return _x;
	}

	public void set_x(double _x) {
		this._x = _x;
	}

	public double get_y() {
		return _y;
	}

	public void set_y(double _y) {
		this._y = _y;
	}
}
