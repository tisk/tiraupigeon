import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.locks.ReentrantLock;

public class Game extends Thread
{
	private class TargetData
	{
		public TargetData(double[] dir, boolean isPull)
		{
			this.dir = dir;
			this.isPull = isPull;
		}
		
		public double[] dir;
		boolean isPull;
	}
	
	public enum e_state
	{
		STATE_CHANGE_STATION,
		STATE_CHANGE_PLAYER,
		STATE_END,
		STATE_READY,
		STATE_SHOOT
	};
	public static final int screenWidth = 1024;
	public static final int screenHeight = 768;
	
	@SuppressWarnings("unchecked")
	private List<TargetData>[] _targetData = new LinkedList[4];
	private e_state _state = e_state.STATE_CHANGE_PLAYER;
	private final int _waitTime = 10000;
	private long _time = 0;
	private Room _room = null;
	private int _station = 1;
	private int _playerTurn = 0;
	private int _turn = 0;
	private final ReentrantLock _listMutex = new ReentrantLock();
	private List<Target> _targets = new LinkedList<Target>();
	private Client _client;
	private boolean _end;
	
	public Game(Room room, Client client)
	{
		_room = room;
		_client = client;
		_playerTurn = 0;
		initTargetData();
	}
	
	private void initTargetData()
	{
		int nbPull = 0;
		int nbMark = 0;
		int nbDouble = 0;
		List<Integer> types = new LinkedList<Integer>();
		for (int i = 0; i < 4; ++i)
			_targetData[i] = new LinkedList<TargetData>();
		for (int i = 0; i < 3; ++i)
			types.add(i);
		for (int i = 0; i < 8; ++i)
			switch (types.get((int)(types.size() * Math.random())))
			{
			case 0:
				_targetData[0].add(new TargetData(new double[]{Math.random() * 45}, true));
				if (nbPull++ == 3)
					types.remove(new Integer(0));
				break;
			case 1:
				_targetData[0].add(new TargetData(new double[]{180 - Math.random() * 45}, false));
				if (nbMark++ == 3)
					types.remove(new Integer(1));
				break;
			case 2:
				_targetData[0].add(new TargetData(new double[]{Math.random() * 45,
						180 - Math.random() * 45}, false));
				if (nbDouble++ == 2)
					types.remove(new Integer(2));
				break;
			}
		for (int i = 0; i < _targetData[0].size(); ++i)
			for (int j = 1; j < 4; ++j)
				_targetData[j].add((int)(Math.random() * (_targetData[j].size() + 1)),
						_targetData[0].get(i));
	}
	
	private void nextPlayer()
	{
		_state = e_state.STATE_CHANGE_PLAYER;
		_turn = (_turn + 1) % 4;
		_playerTurn = (_playerTurn + _turn) % 4;
		if (_turn == 0)
		{
			_station++;
			_playerTurn = 0; // (int)(Math.random() * 4);
			_state = e_state.STATE_CHANGE_STATION;
			_client.changeStation(_station);
		}
		_client.changePlayerTurn(_room.GetTeam(Room.TeamNumber.values()[_playerTurn / 2]).GetPlayerList()[_playerTurn % 2].GetName());
	}
	
	private double sqpw(double x)
	{
		return x * x;
	}
	
	public void Shoot(int x, int y)
	{
		_listMutex.lock();
		int i = 0;
		while (i < _targets.size())
		{
			Target target = _targets.get(i);
			if (sqpw(Target.radius) < sqpw(x - target.get_x()) + sqpw(y - target.get_y()))
				i++;
			else
			{
				_client.killTarget(i);
				_targets.remove(target);
				Player player = _room.GetTeam(Room.TeamNumber.values()[_playerTurn / 2]).GetPlayerList()[_playerTurn % 2];
				player.SetScore(player.GetScore() + 1);
				int score = player.GetScore();
				score += _room.GetTeam(Room.TeamNumber.values()[_playerTurn / 2]).GetPlayerList()[(_playerTurn + 1) % 2].GetScore();
				_client.changeScoreTeam(score, _room.GetTeam(Room.TeamNumber.values()[_playerTurn / 2]).GetPlayerList()[_playerTurn % 2].GetName());
			}
		}
		_listMutex.unlock();
	}
	
	public void Fire()
	{
		createTarget();
		_time = 0;
	}
	
	private void createTarget()
	{
		_state = e_state.STATE_SHOOT;
		TargetData data = _targetData[_playerTurn].get(_station - 1);
		if (data.dir.length != 2)
			_targets.add(new Target(_targets.size(), data.dir[0], data.isPull));
		else
		{
			_targets.add(new Target(_targets.size(), data.dir[0], true));
			_targets.add(new Target(_targets.size(), data.dir[1], false));
		}
		_client.Fire((data.dir.length == 2 ? true : false), data.isPull);
	}
	
	private void manageTarget()
	{
		Date date = new Date();
		if (date.getTime() < _time)
			return ;
		_time = date.getTime() + 17; // ~60FPS
		_listMutex.lock();
		int i = 0;
		while (i < _targets.size())
		{
			Target target = _targets.get(i);
			target.updatePosition();
			if (0 <= target.get_x() + Target.radius && target.get_x() - Target.radius <= screenWidth &&
				0 <= target.get_y() + Target.radius && target.get_y() - Target.radius <= screenHeight)
			{
				// /* DEBUG */ System.out.println("MOVE: " +
				// 		Double.toString(target.get_x()) + ":" + Double.toString(target.get_y()));
				i++;
				_client.mvtTarget(i, (int)target.get_x(), (int)target.get_y());
			}
			else
			{
				// /* DEBUG */ System.out.println("REMOVE: " +
				// 			Double.toString(target.get_x()) + ":" + Double.toString(target.get_y()));
				_targets.remove(target);
				_client.outTarget(i);
			}
		}
		if (i == 0)
			_state = e_state.STATE_END;
		_listMutex.unlock();
	}
	
	private void waiting()
	{
		Date date = new Date();
		if (_time < date.getTime())
		{
			if (_state == e_state.STATE_READY)
			{ // TO CHANGE : Passer le joueur.
				Fire();
				_state = e_state.STATE_SHOOT;
			}
			else
			{
				_time = date.getTime() + _waitTime;
				_state = (_state == e_state.STATE_CHANGE_PLAYER ?
						e_state.STATE_READY : e_state.STATE_CHANGE_PLAYER);
			}
		}
	}
	
	public String getCurrentPlayerName()
	{
		return  _room.GetTeam(Room.TeamNumber.values()[_playerTurn / 2]).GetPlayerList()[_playerTurn % 2].GetName();
	}
	
	public int getPlayerTurn()
	{
		return _playerTurn;
	}
	
	public e_state get_state()
	{
		return _state;
	}
	
	public int get_station()
	{
		return _station;
	}
	
	public void run()
	{
		_end = true;
		_client.changeStation(0);
		_client.changePlayerTurn(_room.GetTeam(Room.TeamNumber.values()[_playerTurn / 2]).GetPlayerList()[_playerTurn % 2].GetName());
		/* DEBUG */ int oldStation = 0;
		/* DEBUG */ e_state oldState = e_state.STATE_END;
		while (_station != 9 && _end == true)
		{
			///* DEBUG */ if (oldStation != _station)
			///* DEBUG */ 	System.out.println("Station: " + Integer.toString(_station));
			///* DEBUG */ if (oldState != _state)
			///* DEBUG */ 	System.out.println("State: " + _state.name());
			///* DEBUG */ oldStation = _station;
			///* DEBUG */ oldState = _state;
			switch (_state)
			{
			case STATE_CHANGE_STATION:
			case STATE_CHANGE_PLAYER:
			case STATE_READY:
				waiting();
				break;
			case STATE_SHOOT:
				manageTarget();
				break;
			case STATE_END:
				nextPlayer();
			}
		}
		int score1 = _room.GetTeam(Room.TeamNumber.TEAM_ONE).GetScore();
		int score2 = _room.GetTeam(Room.TeamNumber.TEAM_TWO).GetScore();
		int teamWin;
		if (score1 > score2)
			teamWin = 1;
		else if (score2 > score1)
			teamWin = 2;
		else
			teamWin = 0;
		_client.sendEndGame(score1, score2, teamWin);
	}
	
	public void endGame()
	{
		_end = false;
	}
	
	/* R�ception des de la partie donn�es du serveur d�di� uniquement a l'utilisation dans l'affichage et/ou effet sonores*/
	
	public void playerShootReceive(int x, int y)
	{
		
	}
	
	public void EndGameReceive(int scoreTeam1, int scoreTeam2, int teamWin)
	{
		_end = false;
	}
	
	public void MvtTargetReceive(int id, int x, int y)
	{
		if (id < _targets.size())
		{
			_targets.get(id).set_x(x);
			_targets.get(id).set_y(y);
		}
	}
	
	public void KillTargetReceive(int id)
	{
		if (id < _targets.size())
			_targets.remove(_targets.get(id));
	}
	
	public void OutTargetReceive(int id)
	{
		if (id < _targets.size())
			_targets.remove(_targets.get(id));
	}
	
	public void ChangeScoreReceive(String player, int score)
	{
		if (_room.GetTeam(Room.TeamNumber.TEAM_ONE).GetPlayer(Team.PlayerType.PLAYER_ONE).GetName() == player)
			_room.GetTeam(Room.TeamNumber.TEAM_ONE).GetPlayer(Team.PlayerType.PLAYER_ONE).SetScore(score);
		if (_room.GetTeam(Room.TeamNumber.TEAM_ONE).GetPlayer(Team.PlayerType.PLAYER_TWO).GetName() == player)
			_room.GetTeam(Room.TeamNumber.TEAM_ONE).GetPlayer(Team.PlayerType.PLAYER_TWO).SetScore(score);
		if (_room.GetTeam(Room.TeamNumber.TEAM_TWO).GetPlayer(Team.PlayerType.PLAYER_ONE).GetName() == player)
			_room.GetTeam(Room.TeamNumber.TEAM_TWO).GetPlayer(Team.PlayerType.PLAYER_ONE).SetScore(score);
		if (_room.GetTeam(Room.TeamNumber.TEAM_TWO).GetPlayer(Team.PlayerType.PLAYER_TWO).GetName() == player)
			_room.GetTeam(Room.TeamNumber.TEAM_TWO).GetPlayer(Team.PlayerType.PLAYER_TWO).SetScore(score);
	}
	
	public void PlayerTurnReceive(String player)
	{
		if (_room.GetTeam(Room.TeamNumber.TEAM_ONE).GetPlayer(Team.PlayerType.PLAYER_ONE).GetName() == player)
			_playerTurn = 0;
		if (_room.GetTeam(Room.TeamNumber.TEAM_ONE).GetPlayer(Team.PlayerType.PLAYER_TWO).GetName() == player)
			_playerTurn = 1;
		if (_room.GetTeam(Room.TeamNumber.TEAM_TWO).GetPlayer(Team.PlayerType.PLAYER_ONE).GetName() == player)
			_playerTurn = 2;
		if (_room.GetTeam(Room.TeamNumber.TEAM_TWO).GetPlayer(Team.PlayerType.PLAYER_TWO).GetName() == player)
			_playerTurn = 3;
	}

	
	public void PosForShootReceive(int pos)
	{
		_station = pos;
	}
	
	public void PullReceive()
	{
		_targets.add(new Target(_targets.size(), 45, true));
	}
	
	public void MarkReceive()
	{
		_targets.add(new Target(_targets.size(), 90, false));
	}
	
	public void DoubleReceive()
	{
		_targets.add(new Target(_targets.size(), 45, true));
		_targets.add(new Target(_targets.size(), 90, false));
	}

	public String toString()
	{
		String send = "";
		String name = _room.GetTeam(Room.TeamNumber.values()[_playerTurn / 2]).GetPlayerList()[_playerTurn % 2].GetName();
		
		send += "Station = " + Integer.toString(_station) + "  -  PlayerTurn : " + name;
		for (int i = 0; i < _targets.size(); i++)
		{
			send += " \n" + Integer.toString(i) + "e Target";
			send += "id : " + Integer.toString(_targets.get(i).get_id());
			send += " - pos : " + Integer.toString((int)_targets.get(i).get_x()) + "/" + Integer.toString((int)_targets.get(i).get_y());
		}
		send += "\n";
		return send;
	}
}
