import java.lang.String;

public class Player {

	private String _name;
	private int _score;
	
	public Player() { _score = 0; _name = "";}
	
	public String GetName() { return _name; }
	public void SetName(String name){ _name = name;}
	public int GetScore() {return _score;}
	public void SetScore(int score) {_score = score;}
	
}
