
public class Network
{
	public enum gameState
	{
		Room,
		Game,
	}
	
	ANetworkModel _networkModel;
	gameState _gameState;
	
	public Network()
	{
		_networkModel = null;
		_gameState = gameState.Room;
	}
	
	public boolean createServer(int port)
	{
		_networkModel =  new NetworkServer(port, this);
		if (_networkModel.connect() == true)
		{
			_networkModel.start();
			return true;
		}
		return false;
	}

	public boolean connectToServer(String adresse, int port)
	{
		_networkModel =  new NetworkClient(adresse, port, this);
		if (_networkModel.connect() == true)
		{
			_networkModel.start();
			return true;
		}
		return false;
	}

	public void clientDisconnect(String explain)
	{
	    System.out.printf("Client Disconnect : %s\n", explain);
	    _networkModel = null;
	}

	public void serverKill(String explain)
	{
	    System.out.printf("Server stop : %s\n", explain);
	    _networkModel = null;
	}

	public void setGameState(gameState gameState)
	{
		_gameState = gameState;
	}
	
	public gameState getGameState()
	{
		return _gameState;
	}
}
