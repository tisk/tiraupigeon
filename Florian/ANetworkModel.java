
public abstract class ANetworkModel extends Thread
{
	protected boolean _keepRunning;
	
	public boolean connect()
	{
		return false;
	}
	
	public boolean getKeepRunning()
	{
		return _keepRunning;	
	}
	
	public void setKeepRunning(boolean state)
	{
		_keepRunning = state;
	}

	public void sendMsg(NetworkMsg msg)
	{
	}
}
