import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;


public class NetworkClient extends ANetworkModel
{
	int _port;
	String _adresse;
	Socket _socket;
	Network _network;
	
	public NetworkClient(String adresse, int port, Network network)
	{
		_port = port;
		_adresse = adresse;
		_socket = null;
		_keepRunning = false;
		_network = network;
	}
	
	public final boolean connect()
	{
		try 
		{
			_socket = new Socket(_adresse, _port);
			return true;
		}
   		catch(IOException e)
		{
   			return false;
		}
	}

	public void run()
	{
		_keepRunning = true;
		while (_socket != null && _keepRunning == true)
		{
			try
			{
				InputStream iStream = _socket.getInputStream();
				
				byte[] b = new byte[1000];
				int bitsRecus = iStream.read(b);
				System.out.printf("Size : %d\n", bitsRecus);
				if(bitsRecus > 0)
				{
					NetworkMsg msg = NetworkMsg.fromBytes(b, bitsRecus);
					usingReceiveMsg(msg);
				}
			}
	   		catch(IOException e)
			{
	   			_keepRunning = false;
	   			_network.clientDisconnect("Host disconnect.");
			}
		}
	}

	private void usingReceiveMsg(NetworkMsg msg)
	{
		//System.out.println("Msg recu : " + msg.toString());
		if (_network.getGameState() == Network.gameState.Room)
			usingReceiveMsgRoom(msg);
		else if (_network.getGameState() == Network.gameState.Game)
			usingReceiveMsgGame(msg);
	}
	
	private void usingReceiveMsgRoom(NetworkMsg msg)
	{
		switch (msg.getHeader())
		{
		case OK :
			break;
		case KO :
			break;
		case KickPlayerRoom :
			kickPlayerRoom();
			break;
		case ListPlayerRoom :
			listPlayerRoom(msg);
			break;
		case ChangeNamePlayer :
			changeNamePlayer(msg);
			break;
		case ChangeTeamPlayer :
			changeTeamPlayer(msg);
			break;
		case DeletedRoom :
			deletedRoom();
			break;
		case RunGame :
			runGame();
			break;
		default :
			break;
		}
	}
	
	private void usingReceiveMsgGame(NetworkMsg msg)
	{
		switch (msg.getHeader())
		{
		case Shoot :
			break;
		case MvtTarget :
			break;
		case KillTarget :
			break;
		case OutTarget :
			break;
		case ChangeScore :
			break;
		case PlayerTurn :
			break;
		case EndGame :
			break;
		case PosForShoot :
			break;
		case Pull :
			break;
		case Mark :
			break;
		case Double :
			break;
		default :
			break;
		}
	}
	
	private void kickPlayerRoom()
	{
		_network.clientDisconnect("Kick from the room.");
	}

	private void changeNamePlayer(NetworkMsg msg)
	{
		String name = "";

		for (int i = 0; i < msg.getData().length; i++)
			name += (char)msg.getData()[i];
	}
	
	private void changeTeamPlayer(NetworkMsg msg)
	{
		int team = NetworkMsg.bytesToInt(msg.getData(), 0);
	}
	
	private void listPlayerRoom(NetworkMsg msg)
	{
		int lenght1, lenght2, lenght3, lenght4;
		String namePlayer1Team1 = "", namePlayer2Team1 = "", namePlayer1Team2 = "", namePlayer2Team2 = "";

		lenght1 = NetworkMsg.bytesToInt(msg.getData(), 0);
		lenght2 = NetworkMsg.bytesToInt(msg.getData(), 8);
		lenght3 = NetworkMsg.bytesToInt(msg.getData(), 16);
		lenght4 = NetworkMsg.bytesToInt(msg.getData(), 24);

		System.out.printf("Test : %d-%d-%d-%d\n", lenght1, lenght2, lenght3, lenght4);
		
		for (int i = 32; i < (32 + lenght1); i++)
			namePlayer1Team1 += (char)msg.getData()[i];
		for (int i = (32 + lenght1); i < (32 + lenght1 + lenght2); i++)
			namePlayer2Team1 += (char)msg.getData()[i];
		for (int i = (32 + lenght1 + lenght2); i < (32 + lenght1 + lenght2 + lenght3); i++)
			namePlayer1Team2 += (char)msg.getData()[i];
		for (int i = (32 + lenght1 + lenght2 + lenght3); i < (32 + lenght1 + lenght2 + lenght3 + lenght4); i++)
			namePlayer2Team2 += (char)msg.getData()[i];

		System.out.println("List Player recu  : Team 1 : " + namePlayer1Team1 + " - " + namePlayer2Team1 + "\nTeam 2 :  " + namePlayer1Team2 + " - " + namePlayer2Team2);
	}
	
	private void deletedRoom()
	{
		_network.clientDisconnect("Room deleted.");
	}
	
	private void runGame()
	{
		// TO DO Run Game
	}
	
	public void sendMsg(NetworkMsg msg)
	{
		byte[] send = msg.arrayByteToSend();

		try
		{
			OutputStream oStream = _socket.getOutputStream();
			
			oStream.write(send);
		}
   		catch(IOException e)
		{
   			_keepRunning = false;
   			_network.clientDisconnect("Host didn't respond.");
		}
	}
}
