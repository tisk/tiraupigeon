import java.io.IOException;

public class Main_Test
{
	static Network _network;
	static boolean flagEnd;
	
	private static void act(String cmd)
	{
	    System.out.printf("cmd = %s$\n", cmd);
		if (cmd.length() == 4 && cmd.compareTo("quit") == 0)
			flagEnd = true;
		if (cmd.length() >= 16 && cmd.substring(0, 6).compareTo("client") == 0)
		{
			String s = new String(""), s2 = new String("");
			
			int pos = 6;
			while (cmd.charAt(pos) == ' ')
				pos++;
			while (pos < cmd.length() && cmd.charAt(pos) != ' ')
				s += cmd.charAt(pos++);
			while (cmd.charAt(pos) == ' ')
				pos++;
			while (pos < cmd.length() && cmd.charAt(pos) != ' ')
				s2 += cmd.charAt(pos++);
			boolean flag = _network.connectToServer(s, Integer.decode(s2));
			if (flag == true)
			    System.out.printf("Connect to server\n");
			else
				System.out.printf("Connect to server Failed\n");
				
		}
		if (cmd.length() >= 8 && cmd.substring(0, 6).compareTo("server") == 0)
		{
			String s = new String("");
			
			int pos = 6;
			while (cmd.charAt(pos) == ' ')
				pos++;
			while (pos < cmd.length() && cmd.charAt(pos) != ' ')
				s += cmd.charAt(pos++);
			if (_network.createServer(Integer.decode(s)) == true)
			    System.out.printf("Create server\n");
			else
			    System.out.printf("Create server Failed\n");
		}
	}
	
	  public static void main(String[] args)
	  {
		  String cmd = new String("");
		  _network = new Network();
		  
		  flagEnd = false;
		  while (flagEnd == false)
		  {
			  try
			  {
				  byte[] array = new byte[200];
				  int i = System.in.read(array);
				  cmd = "";
				  for (int j = 0; j < (i - 2); j++)
				  {
				   	cmd += "" +(char)array[j];
				  }
				  act(cmd);
			  }
	          catch(IOException e)
			  {
	      	    System.out.println("Error");
			  }
		  }
	  }
}
