import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;


public class NetworkServer extends ANetworkModel
{
	int _port;
	ServerSocket _server;
	Socket _client;
	ArrayList<ClientConnected> _list;
	Network _network;
	
	public NetworkServer(int port, Network network)
	{
		_port = port;
		_server = null;
		_client = null;
		_keepRunning = false;
		_list = new ArrayList<ClientConnected>();
		_network = network;
	}
	
	private String findName(int i)
	{
		String name = "Player " + Integer.toString(i);
		
		for (int j = 0; j < _list.size(); j++)
		{
			if (_list.get(j).getNameClient().compareTo(name) == 0)
				return findName(i + 1);
		}
		return name;
	}
	
	public boolean nameAlreadyExist(String name)
	{
		for (int j = 0; j < _list.size(); j++)
		{
			if (_list.get(j).getNameClient().compareTo(name) == 0)
				return false;
		}
		return true;
	}

	private int findTeam()
	{
		int nbTeam1 = 0;
		
		for (int j = 0; j < _list.size(); j++)
		{
			if (_list.get(j).getTeam() == 1)
				nbTeam1++;
		}
		if (nbTeam1 == 2)
			return 2;
		return 1;
	}
	
	public boolean freePlaceInTeam(int team)
	{
		int nbTeam1 = 0;
		int nbTeam2 = 0;
		
		for (int j = 0; j < _list.size(); j++)
		{
			if (_list.get(j).getTeam() == 1)
				nbTeam1++;
			if (_list.get(j).getTeam() == 2)
				nbTeam2++;
		}
		if (team == 1 && nbTeam1 < 2)
			return true;
		if (team == 2 && nbTeam2 < 2)
			return true;
		return false;
	}
	
	public final boolean connect()
	{
	   try
	   {
			_server = new ServerSocket(_port);
			return true;
		}
   		catch(IOException e)
		{
			System.out.printf("Server Failed : %s\n", e.toString());
   			return false;
		}
	}

	public void updateRoomInfo()
	{
		String namePlayer1Team1 = "";
		String namePlayer2Team1 = "";
		String namePlayer1Team2 = "";
		String namePlayer2Team2 = "";
		int flag1 = 0, flag2 = 0;

		for (int j = 0; j < _list.size(); j++)
		{
			if (_list.get(j).getTeam() == 1)
			{
				flag1++;
				if (flag1 == 1)
					namePlayer1Team1 = _list.get(j).getNameClient();
				else
					namePlayer2Team1 = _list.get(j).getNameClient();
			}
			if (_list.get(j).getTeam() == 2)
			{
				flag2++;
				if (flag2 == 1)
					namePlayer1Team2 = _list.get(j).getNameClient();
				else
					namePlayer2Team2 = _list.get(j).getNameClient();
			}
		}
		System.out.printf("Test : %d-%d-%d-%d : %s-%s-%s-%s\n", namePlayer1Team1.length(), namePlayer2Team1.length(), namePlayer1Team2.length(), namePlayer2Team2.length(),
				namePlayer1Team1, namePlayer2Team1, namePlayer1Team2, namePlayer2Team2);
		byte[] toSend = new byte[32 + namePlayer1Team1.length() + namePlayer2Team1.length() + namePlayer1Team2.length() + namePlayer2Team2.length()];
		System.arraycopy(NetworkMsg.intToBytes(namePlayer1Team1.length()), 0, toSend, 0, 8);
		System.arraycopy(NetworkMsg.intToBytes(namePlayer2Team1.length()), 0, toSend, 8, 8);
		System.arraycopy(NetworkMsg.intToBytes(namePlayer1Team2.length()), 0, toSend, 16, 8);
		System.arraycopy(NetworkMsg.intToBytes(namePlayer2Team2.length()), 0, toSend, 24, 8);
		flag1 = 32;
		System.arraycopy(namePlayer1Team1.getBytes(), 0, toSend, flag1, namePlayer1Team1.getBytes().length);
		flag1 += namePlayer1Team1.getBytes().length;
		System.arraycopy(namePlayer2Team1.getBytes(), 0, toSend, flag1, namePlayer2Team1.getBytes().length);
		flag1 += namePlayer2Team1.getBytes().length;
		System.arraycopy(namePlayer1Team2.getBytes(), 0, toSend, flag1, namePlayer1Team2.getBytes().length);
		flag1 += namePlayer1Team2.getBytes().length;
		System.arraycopy(namePlayer2Team2.getBytes(), 0, toSend, flag1, namePlayer2Team2.getBytes().length);
		System.out.printf("Test2 : %d\n", toSend.length);
		sendMsg(new NetworkMsg(NetworkMsg.msgHeader.ListPlayerRoom, toSend));
	}
	
	private void sendHisOwnInfo(ClientConnected player)
	{
		player.sendMsg(new NetworkMsg(NetworkMsg.msgHeader.ChangeNamePlayer, player.getNameClient().getBytes()));
		player.sendMsg(new NetworkMsg(NetworkMsg.msgHeader.ChangeTeamPlayer, NetworkMsg.intToBytes(player.getTeam())));
	}
	
	public boolean isHost(ClientConnected player)
	{
		if (_list.size() == 0 || _list.get(0) != player)
			return false;
		return true;
	}

	public void deleteRoom()
	{
		sendMsg(new NetworkMsg(NetworkMsg.msgHeader.DeletedRoom, new byte[0]));
		_network.serverKill("Room deleted");
	}
	
	public void run()
	{
		_keepRunning = true;
		while (_keepRunning == true)
		{
			try
			{
				Socket client = _server.accept();
				if (_list.size() < 3)
				{
					_list.add(new ClientConnected(client, findName(1), findTeam(), this));
					_list.get(_list.size() - 1).start();
					sendHisOwnInfo(_list.get(_list.size() - 1));
					updateRoomInfo();
				}
				System.out.printf("Server Run, Client connected\n");
			}
	   		catch(IOException e)
			{
				System.out.printf("Server Failed : %s\n", e.toString());
			}
		}
	}

	public void sendMsg(NetworkMsg msg)
	{
		for (int i = 0; i < _list.size(); i++)
			_list.get(i).sendMsg(msg);
	}

	public void sendMsg(NetworkMsg msg, ClientConnected player)
	{
		player.sendMsg(msg);
	}

	public void sendMsg(NetworkMsg msg, String name)
	{
		for (int i = 0; i < _list.size(); i++)
			if (_list.get(i).getNameClient().compareTo(name) == 0)
				_list.get(i).sendMsg(msg);
	}
	
	public void kickPlayer(String name)
	{
		for (int i = 0; i < _list.size(); i++)
			if (_list.get(i).getNameClient().compareTo(name) == 0)
			{
				_list.get(i).setKeepRunning(false);
				disconnectClient(_list.get(i));
			}
	}
	
	public void disconnectClient(ClientConnected client)
	{
		_list.remove(client);
		if (_network.getGameState() == Network.gameState.Room)
			updateRoomInfo();
	}
	
	public Network getNetwork()
	{
		return _network;
	}
}
