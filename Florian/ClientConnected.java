import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;


public class ClientConnected extends Thread
{
	Socket _client;
	String _nameClient;
	boolean _keepRunning;
	int _team;
	NetworkServer _server;

	public ClientConnected(Socket client, int team, NetworkServer server)
	{
		_client = client;
		_nameClient = "";
		_keepRunning = false;
		_server = server;
		_team = team;
	}

	public ClientConnected(Socket client, String name, int team, NetworkServer server)
	{
		_client = client;
		_nameClient = name;
		_keepRunning = false;
		_server = server;
		_team = team;
	}
	
	public void run()
	{
		_keepRunning = true;
		while (_client != null && _keepRunning == true)
		{
			try
			{
				InputStream iStream = _client.getInputStream();
				
				byte[] b = new byte[1000];
				int bitsRecus = iStream.read(b);
				if (bitsRecus > 0)
				{
					NetworkMsg msg = NetworkMsg.fromBytes(b, bitsRecus);
					usingReceiveMsg(msg);
				}
			}
	   		catch(IOException e)
			{
				_server.disconnectClient(this);
				_keepRunning = false;
			}
		}
	}

	private void usingReceiveMsg(NetworkMsg msg)
	{
		System.out.println("Msg recu : " + msg.toString());
		if (_server.getNetwork().getGameState() == Network.gameState.Room)
			usingReceiveMsgRoom(msg);
		else if (_server.getNetwork().getGameState() == Network.gameState.Game)
			usingReceiveMsgGame(msg);
	}
	
	private void usingReceiveMsgRoom(NetworkMsg msg)
	{
		switch (msg.getHeader())
		{
		case ChangeTeamPlayer :
			changedTeam(msg);
			break;
		case ChangeNamePlayer :
			changedName(msg);
			break;
		case RunGame :
			runGame();
			break;
		case DeletedRoom :
			deleteRoom(msg);
			break;
		case KickPlayerRoom :
			kickPlayer(msg);
			break;
		default :
			break;
		}
	}
	
	private void usingReceiveMsgGame(NetworkMsg msg)
	{
		switch (msg.getHeader())
		{
		case Shoot :
			break;
		case Pull :
			break;
		case Mark :
			break;
		case Double :
			break;
		default :
			break;
		}
	}
	
	private void runGame()
	{
		if (_server.isHost(this)  == false)
			sendMsg(new NetworkMsg(NetworkMsg.msgHeader.KO, new byte[0]));
		else
		{
			sendMsg(new NetworkMsg(NetworkMsg.msgHeader.OK, new byte[0]));
			// TO_DO RUN GAME
		}
	}
	
	private void deleteRoom(NetworkMsg msg)
	{
		if (_server.isHost(this)  == false)
			sendMsg(new NetworkMsg(NetworkMsg.msgHeader.KO, new byte[0]));
		else
		{
			sendMsg(new NetworkMsg(NetworkMsg.msgHeader.OK, new byte[0]));
			_server.deleteRoom();
		}
	}
	
	private void kickPlayer(NetworkMsg msg)
	{
		byte[] data = msg.getData();
		String name = "";
		
		for (int i = 0; i < data.length; i++)
			name += (char)data[i];
		if (_server.isHost(this)  == false|| name.length() <= 0 || _server.nameAlreadyExist(name) == false)
			sendMsg(new NetworkMsg(NetworkMsg.msgHeader.KO, new byte[0]));
		else
		{
			sendMsg(new NetworkMsg(NetworkMsg.msgHeader.OK, new byte[0]));
			_server.kickPlayer(name);
		}
	}
	
	private void changedName(NetworkMsg msg)
	{
		byte[] data = msg.getData();
		String name = "";
		
		for (int i = 0; i < data.length; i++)
			name += (char)data[i];
		if (name.length() <= 0 || _server.nameAlreadyExist(name) == true)
			sendMsg(new NetworkMsg(NetworkMsg.msgHeader.KO, new byte[0]));
		else
		{
			_nameClient = name;
			sendMsg(new NetworkMsg(NetworkMsg.msgHeader.OK, new byte[0]));
		}
		_server.updateRoomInfo();
	}
	
	private void changedTeam(NetworkMsg msg)
	{
		int requestTeam = NetworkMsg.bytesToInt(msg.getData(), 0);
		
		if (_server.freePlaceInTeam(requestTeam) == true)
		{
			_team = requestTeam;
			sendMsg(new NetworkMsg(NetworkMsg.msgHeader.OK, new byte[0]));
		}
		else
			sendMsg(new NetworkMsg(NetworkMsg.msgHeader.KO, new byte[0]));
		_server.updateRoomInfo();
	}
	
	public void sendMsg(NetworkMsg msg)
	{
		byte[] send = msg.arrayByteToSend();

		try
		{
			OutputStream oStream = _client.getOutputStream();

			oStream.write(send);
		}
   		catch(IOException e)
		{
			System.out.println("Client dead");
			_server.disconnectClient(this);
			_keepRunning = false;
		}
	}

	public String getNameClient()
	{
		return _nameClient;	
	}
	
	public void setNameClient(String name)
	{
		_nameClient = name;
	}

	public int getTeam()
	{
		return _team;	
	}
	
	public void setTeam(int team)
	{
		_team = team;
	}
	
	public boolean getKeepRunning()
	{
		return _keepRunning;	
	}
	
	public void setKeepRunning(boolean state)
	{
		_keepRunning = state;
	}
}
